# Fediverse Enhancement Proposals

This is the Git repository containing Fediverse Enhancment Proposals (FEPs).

# Submitting 

TODO a short description on how submit.

For more information see [FEP-a4edc7b](./feps/fep-a4edc7b.md).

# Editors

Editors are listed in the [EDITORS](EDITORS) file.

# Copyright

CC0 1.0 Universal (CC0 1.0) Public Domain Dedication 

To the extent possible under law, the authors of this document have waived all copyright and related or neighboring rights to this work.
